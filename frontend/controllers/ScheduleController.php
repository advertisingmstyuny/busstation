<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05.03.2018
 * Time: 14:59
 */

namespace frontend\controllers;


use backend\models\Bank;
use backend\models\OrderProfile;
use backend\models\Customer;
use backend\models\Order;
use backend\models\Route;
use backend\models\RouteStation;
use backend\models\Schedule;
use backend\models\Station;
use frontend\models\Profile;
use yii\data\ArrayDataProvider;
use yii\db\Schema;
use yii\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;
use kartik\mpdf\Pdf;
class ScheduleController extends Controller
{
    public function actionRoute()
    {
        $qq = Yii::$app->request->get('term');
        $query = Station::find()->select(['title as value'])->where(['like','title', $qq])
            ->asArray('title')
            ->all();
        $result = array();
        foreach($query as $q){
        if (strstr($q['value'], $qq)) {
            $result[] = array('value' => $q['value']);
        }
    }
        return json_encode($result);
    }

    public function actionRoutet()
    {
        $qq = Yii::$app->request->get('term');
        $query = Station::find()->select(['title as value'])->where(['like','title', $qq])
            ->asArray('title')
            ->all();
        $result = array();
        foreach($query as $q){
            if (strstr($q['value'], $qq)) {
                $result[] = array('value' => $q['value']);
            }
        }
        return json_encode($result);
    }

    public function actionSearch()
    {
        $departure_c = Yii::$app->request->post('departure');
        $arrival_c = Yii::$app->request->post('arrival');
        $departure_date_c = Yii::$app->request->post('departure_date');

        $departure_id = Station::findOne(['title' => $departure_c])->id;
        $arrival_id = Station::findOne(['title' => $arrival_c])->id;

        $departure = RouteStation::find()->where(['id_station' => $departure_id])->asArray()->all();
        $arrival = RouteStation::find()->where(['id_station' => $arrival_id])->asArray()->all();

        $schedule = Schedule::find()->asArray()->all();

        $model = new Schedule();

//        var_dump($date);die();

        $departure_date = explode('/', $departure_date_c);
//
//        if ($date[0] % 2) {
//            var_dump($date[0]);
//        }

//        die();

        $result = [];
        foreach($schedule as $s){
            foreach ($departure as $d){
                foreach ($arrival as $a){
                    if ($s['id_route'] == $d['id_route'] && $s['id_route'] == $a['id_route'])
                    {
                        if ($d['id'] < $a['id']) {
                            if ($departure_date[0] % 2) {
                                if ($s['mode'] == 1 || $s['mode'] == 0){
                                    $result[] = $s;
                                }
                            } else if ($departure_date[0] % 2 == false) {
                                if ($s['mode'] == 2 || $s['mode'] == 0) {
                                    $result[] = $s;
                                }
                            }
                        }
                    }
                }
            }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $result
        ]);

        return $this->render('search', [
            'model' => $model,
            'departure_c' => $departure_c,
            'arrival_c' => $arrival_c,
            'departure_date' => $departure_date_c,
            'dataProvider' => $dataProvider
        ]);
    }
public function actionScheduleView($id){
        $city = Yii::$app->request->get('city');
        $array_ex = explode(';',$city);
        $model = Schedule::findOne(['id_route'=>$id]);
//        var_dump($array_ex);die();
        if ($array_ex[0] == null)
        {
            $array_ex = explode('-',$model->route->title);
        }
        $departure = Station::findOne(['title'=>$array_ex[0]])->id;
        $arrival = Station::findOne(['title'=>$array_ex[1]])->id;
        $departure = RouteStation::findOne(['id_route'=>$model->id_route, 'id_station'=>$departure])->id;
        $arrival = RouteStation::findOne(['id_route'=>$model->id_route, 'id_station'=>$arrival])->id;
        $date = Yii::$app->request->get('date');
    if($departure<$arrival){
        return $this->redirect([
            'view',
            'id'=>$id,
            'departure'=>$array_ex[0],
            'arrival'=> $array_ex[1],
            'date'=> $date
        ]);
    }
    else{
        return $this->redirect([
            'view',
            'id'=>$id,
            'departure'=>$array_ex[1],
            'arrival'=> $array_ex[0],
            'date'=> $date
        ]);
    }

}
    public function actionView($id)
    {
        $departure = Yii::$app->request->get('departure');
        $arrival = Yii::$app->request->get('arrival');
        $date = Yii::$app->request->get('date');

        $model = Schedule::findOne(['id_route' => $id]);

        foreach ($model->route->routeStations as $rs){
            if ($rs->station->title == $departure)
            {
                $departure_length = $rs->length;
            }
            if ($rs->station->title == $arrival)
            {
                $arrival_length = $rs->length;
            }
        }

        $price = ($model->route->price * ($arrival_length - $departure_length)) / $model->route->length;
        setlocale(LC_MONETARY, 'ua_UA');
        $price = number_format($price, 2, '.', '');

        return $this->render('view', [
            'model' => $model,
            'departure' => $departure,
            'arrival' => $arrival,
            'date' => $date,
            'price' => $price,
        ]);
    }

    public function actionOrder()
    {
            $id = Yii::$app->request->post('id');
            $departure = Yii::$app->request->post('departure');
            $arrival = Yii::$app->request->post('arrival');
            $price = Yii::$app->request->post('price');
            $date = Yii::$app->request->post('date');
            $place = Yii::$app->request->post('place');

        if($price==null){
            $data = Yii::$app->request->post('Customer',[]);
            $price= $data['price'];

        }
        if(is_array($place)){
            $place = implode(';', $place);
        }

        $schedule = Schedule::findOne(['id' => $id]);
        $array = array();

        if ($place == null)
        {
            $session = Yii::$app->session;
            $session->setFlash('errorPlace','Вы не выбрали место.');
            return $this->redirect(
                [
                    'view',
                    'id' => $schedule->id_route,
                    'departure' => $departure,
                    'arrival' => $arrival,
                    'date' => $date
                ]
            );
        }
        else
        {
            foreach ($schedule->orders as $order) {
                if ($order->departure_date == $date){
                    $array_ex = explode(';',$order->place);

                    for ($i = 0; $i < count($array_ex); $i++)
                    {
                        array_push($array, $array_ex[$i]);
                    }
                }
            }

            for ($i = 0; $i < $schedule->vehicleDriver->vehicle->count_places; $i++)
            {
                foreach ($schedule->orders as $order)
                {
                    if (in_array($i+1, $array) == true && $order->departure_date == $date)
                    {
                        $array_place=explode(';',$place);
                        for($j=0; $j<count($array_place); $j++)
                        {
                            if($i+1==$array_place[$j]){
                                $session = Yii::$app->session;
                                $session->setFlash('errorPlace','Это место уже занято.');
                                return $this->redirect(
                                    [
                                        'view',
                                        'id' => $schedule->id_route,
                                        'departure' => $departure,
                                        'arrival' => $arrival,
                                        'date' => $date
                                    ]
                                );
                            }
                        }
                    }
                }

            }
        }

        if (!Yii::$app->user->isGuest)
        {
            return $this->actionFinal($id, $departure, $arrival, $place, $price, $date, $customer=1);
        }

        $model = new Customer();

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->actionFinal($id, $departure, $arrival, $place, $price, $date,$model->id);
        }

        return $this->renderAjax('order',[
            'model' => $model,
            'id' => $id,
                'departure' => $departure,
                'arrival' => $arrival,
                'place' => $place,
                'price' => $price,
                'date' => $date
        ]);
    }

    public function actionFinal($id,$departure,$arrival,$place,$price, $date, $customer)
    {
        if(is_array($place)){
            $place = implode(';', $place);
        }
        $arr_place = explode(';', $place);
        $price *= count($arr_place);
        if (Yii::$app->user->isGuest) {
           return $this->renderAjax('final', [
               'id' => $id,
               'departure' => $departure,
               'arrival' => $arrival,
               'place' => $place,
               'price' => $price,
               'date' => $date,
               'customer' => $customer
            ]);
        } else {


// var_dump('gfdsfgh');
            return $this->renderAjax('order_final', [
                'id' => $id,
               'departure' => $departure,
               'arrival' => $arrival,
               'place' => $place,
               'price' => $price,
               'date' => $date,
               'customer' => $customer
            ]);
        }


    }

    public function actionBuy($id,$departure,$arrival,$place,$price, $date, $customer)
    {

        if($id==null){
            $id = Yii::$app->request->post('id');
            $departure = Yii::$app->request->post('departure');
            $arrival = Yii::$app->request->post('arrival');
            $place = Yii::$app->request->post('place');
            $price = Yii::$app->request->post('price');
            $date = Yii::$app->request->post('date');
            $customer = Yii::$app->request->post('customer');
        }
        $model = new Bank();
//        $model->money=Order::findOne(['id'=>$id])->end_price;
        $model->money=$price;
        $model->created_at=date('d/m/y');

        if($model->load(Yii::$app->request->post())&& $model->save()){
            return $this->redirect(['success', 'id' => $id,
                'departure' => $departure,
                'arrival' => $arrival,
                'place' => $place,
                'price' => $price,
                'date' => $date,
                'customer' => $customer,
                'status' => 1]);
        }
        return $this->render('card', [
            'model' => $model,
            'id' => $id,
            'departure' => $departure,
            'arrival' => $arrival,
            'place' => $place,
            'price' => $price,
            'date' => $date,
            'customer' => $customer

        ]);
    }

    public function actionSuccess($id,$departure,$arrival,$place,$price, $date, $customer, $status=0)
    {
        $model = new Order();
        //        var_dump($id.' '.$departure.' '.$arrival.' '.$place.' '.$date.' '.$price.' '.$customer);die();

        if ($id != null && $departure != null && $arrival != null && $place != null && $price != null)
        {
            $model->id_customer = $customer;
            $model->id_schedule = $id;
            $model->station_departure = $departure;
            $model->station_arrival = $arrival;
            $model->create_at = date('d.m.Y');
            $model->place = $place;
            $model->end_price = $price;
            $model->departure_date = $date;
            $model->status= $status;
            $model->save();
        }
        if(!Yii::$app->user->isGuest){
            $modelProfile = Profile::findOne(['user_id' => Yii::$app->user->identity['id']]);

            $orderProfile = new OrderProfile();
            $orderProfile->id_profile = $modelProfile->id;
            $orderProfile->id_order = $model->id;
            $orderProfile->status = 1;
            $orderProfile->save();
        }
        $content = $this->renderPartial('ticket', [
            'model' => $model,
            // etc...
        ]);
        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => [
                'title' => 'Factuur',
                'subject' => 'Generating PDF files via yii2-mpdf extension has never been easy'
            ],
            'methods' => [
                'SetHeader' => ['Generated By: Krajee Pdf Component||Generated On: ' . date("r")],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        return $pdf->render();
    }

    public function actionDetail()
    {
        $id = Yii::$app->request->get('id');
        $model = Schedule::findOne(['id' => $id]);

        return $this->render('detail', [
            'model' => $model
        ]);
    }

    public function actionPlaces($id)
    {
        $places = Yii::$app->request->post('places');

        return $this->renderAjax('view',[
            'places' => $places
        ]);
    }
}