<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy
 * Date: 26.03.2018
 * Time: 19:08
 */

namespace frontend\controllers;


use backend\models\OrderProfile;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use frontend\models\Profile;
use Yii;
use yii\web\NotFoundHttpException;
class ProfileController extends Controller
{
    public function actionCreate()
    {
        $model = new Profile();

        if (Profile::findOne(['user_id' => Yii::$app->user->identity['id']]) == true){
            return $this->redirect(['view','id' => Yii::$app->user->identity['id']]);
        }

        $model->user_id = Yii::$app->user->identity['id'];

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['view', 'id' => $model->id]);
            }
        return $this->render('create', [
            'model'=>$model
        ]);
}

    public function actionView($id)
    {
        $model = $this->findModel($id);

        $orderModel = OrderProfile::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $orderModel,
            'sort' => [
                'defaultOrder'=>['id'=>'desc']
            ]

        ]);

        if($model->user_id != Yii::$app->user->identity['id']){
            return $this->redirect(['create']);
        }
        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionEdit($id)
    {
     $model = $this->findModel($id);

     if($model->load(Yii::$app->request->post()) && $model->save()){
         return $this->redirect(['view', 'id'=>$id]);
     }
     return $this->render('edit', [
         'model' => $model
     ]);
    }

    protected function findModel($id)
    {
        if(($model = Profile::findOne(['user_id' => $id])) !== null){
            return $model;
        } else {
//            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
//            return null;
        }
    }
}