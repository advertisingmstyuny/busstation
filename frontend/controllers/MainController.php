<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01.03.2018
 * Time: 0:48
 */

namespace frontend\controllers;


use backend\models\News;
use backend\models\Order;
use backend\models\ReturnTicket;
use backend\models\Schedule;
use yii\web\Controller;
use Yii;
use common\models\LoginForm;
use frontend\models\SignupForm;
use yii\data\ActiveDataProvider;
use backend\models\RouteStation;

class MainController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionSchedule()
    {
        $model = new Schedule();
        $query = Schedule::find();
        $sort = Yii::$app->request->get('sort');


        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        if ($sort == 1)
        {
            $query->joinWith(['route'])->orderBy(['number' => SORT_ASC]);
        }
        else if ($sort == 2)
        {
            $query->joinWith(['route'])->orderBy(['number' => SORT_DESC]);
        }
//        else if ($sort == 3)
//        {
////            $query->with('route')->viaTable(RouteStation::tableName(), ['id_route' => 'id'], function($q){
////                $q->distinct('id_route')->orderBy(['time' => SORT_ASC]);
////            });
//            $query->joinWith(['route.routeStations'])->orderBy(['arrival_time' => SORT_ASC]);
////            $query->joinWith(['routeStations'])->orderBy(['time' => SORT_ASC]);
//        }
//        else if ($sort == 4)
//        {
//            $query->joinWith(['route.routeStations'])->orderBy(['arrival_time' => SORT_DESC]);
//        }
//        else if ($sort == 5)
//        {
//            $query->joinWith(['route.routeStations'])->orderBy(['time' => SORT_ASC]);
////            $query->joinWith(['route.routeStations'=>function($query) {
////                return $query->distinct('id_route')->orderBy(['arrival_time' => SORT_ASC]);
////            }]);
//        }
//        else if ($sort == 6)
//        {
//            $query->joinWith(['route.routeStations'])->orderBy(['time' => SORT_DESC]);
//        }

        return $this->render('schedule', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'sort' => $sort
        ]);
    }
    public function actionReturn()
    {
        $model = new ReturnTicket();

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $return_money='Ивините, время истекло';
            if($model->numberTicket->status==1){
                if($model->numberTicket->customer->phone_number==$model->phone || $model->numberTicket->orderProfiles->profile->phone_number==$model->phone){
    //               var_dump('phone');
                    if($model->numberTicket->create_at==$model->date_pay ){
    //                    var_dump('date');
                        if($model->numberTicket->customer->full_name==$model->full_name ||$model->numberTicket->orderProfiles->profile->second_name.' '.$model->numberTicket->orderProfiles->profile->first_name.' '.$model->numberTicket->orderProfiles->profile->third_name==$model->full_name){
    //                        var_dump('name');
                            if($model->numberTicket->status!=4){
    //                            var_dump('final');
                                $model->numberTicket->status=4;
                                $model->numberTicket->save();

                                $date=date('d.m.Y H:i:s');
                                $time='';
                                foreach ($model->numberTicket->schedule->route->routeStations as $station){
                                    if($model->numberTicket->station_departure==$station->station->title){
                                        $time=$station->time;
                                    }
                                }
                                $departure=$model->numberTicket->departure_date.' '.$time;
                                $d1=strtotime($departure);
                                $d2=strtotime($date);
                                $result=($d1-$d2)/60;
                                if($result>=10){
                                    $return_money=$model->numberTicket->end_price*0.8;
                                }
                                if($result>=120){
                                    $return_money=$model->numberTicket->end_price*0.9;
                                }
                                if($result>=1440){
                                    $return_money=$model->numberTicket->end_price;
                                }
                                if($result<0 && $result>=-60 || $result<10){
                                    $return_money=$model->numberTicket->end_price*0.7;
                                }
                            }
                        }
                    }
                }
            }
            else {$return_money='Ивините, вы еще не оплатили билет';}
//            die();
            return $this->redirect(['result', 'money'=>$return_money]);
        }

        return $this->render('return',[
            'model' => $model
        ]);
    }

    public function actionResult($money)
    {
        return $this->render('result',['money'=>$money]);
    }

    public function actionNews()
    {
        $query = News::find()->where(['status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=>['id'=>'desc']]
        ]);

        return $this->render('news',[
            'dataProvider' => $dataProvider
        ]);
    }
    public function actionContacts()
    {
        return $this->render('contacts');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(['/profile/create']);
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}