<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.03.2018
 * Time: 20:12
 */

namespace frontend\controllers;


use backend\models\News;
use yii\web\Controller;
use yii\data\ActiveDataProvider;

class NewsController extends Controller
{
    public function actionView()
    {
//        $data = file_get_contents('../../backend/web/news.txt');
//        $news = unserialize($data);

        $news = News::find()->where(['status' => 2]);

        $provider = new ActiveDataProvider([
            'query' => $news,
        ]);

        $news = $provider->getModels();

        $newsArray = array();

        for ($i = 0; $i < count($news); $i++)
        {
            $newsArray[] = [
                'description' => $news[$i]['description']
            ];
        }

        return json_encode($newsArray);
    }
}