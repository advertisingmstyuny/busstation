<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\ReturnTicket */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Возврат билета';

?>

<div class="wrapper">
    <div class="row">
        <div class="col-2-3">
            <h3>Правила возврата автобусных билетов:</h3>
            <table class ="return-table">
                        <tr>
                            <th>Время оставшееся до отправления автобуса</th>
                            <th>Удержания*</th>
                            <th>Возврат*</th>
                        </tr>
                        <tr>
                            <td>Более 24 часов до отправления</td>
                            <td>0%*</td>
                            <td>100%</td>
                        </tr>
                        <tr>
                            <td>2 часа до момента отправления</td>
                            <td>10%</td>
                            <td>90%</td>
                        </tr>
                        <tr>
                            <td>10 минут до отправления автобуса</td>
                            <td>20%</td>
                            <td>80%</td>
                        </tr>
                        <tr>
                            <td>менее 10 минут, но более трех часов после отправления</td>
                            <td>30%</td>
                            <td>70%</td>
                        </tr>
                    </table>
        </div>
        <div class="col-1-3">
            <p>*В таблице указаны только проценты от стоимости билета. Комиссионные сборы системы Uticket и платежных систем не возвращается.</p>
                <p>*Согласно http://zakon3.rada.gov.ua/laws/show/176-97-%D0%BF  ст.136
                    -Пассажир имеет право на возврат билета в течение трех суток с момента отправления автобуса в случае болезни или несчастного случая, что подтверждается соответствующими документами.
                    В таком случае ему возмещается уплаченная сумма, кроме платы за продажу билетов, или бесплатно переоформляется билет на другой автобус.
                    Если человек заболел и не в состоянии самостоятельно вернуть билет в кассу автостанции, то в таких случаях необходимо действовать, не теряя времени, через третьих лиц.
                </p>
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="return-text">
            Если пассажир по каким-либо причинам не может воспользоваться
            своим автобусным билетом и намерен сдать его и вернуть часть стоимости билета,
            в зависимости от того, какой из видов билетов был приобретен, необходимо заполнить cоответствующую форму.
        </div>
        <div class="col-1-4">
        </div>
        <div class="col-2-4">
            <div class="return-ticket-form">
                <div class="return-form-in">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'number_ticket')->textInput() ?>

                    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'number_card')->textInput() ?>

                    <?= $form->field($model, 'phone')->textInput() ?>

                    <?= $form->field($model, 'date_pay')->widget(DatePicker::classname(),
//        [
//        'type' => DatePicker::TYPE_INLINE,
//        'options' => ['placeholder' => 'Введите дату оплаты ...'],
//        'language' => 'ru',
//        'pluginOptions' => [
////            'autoclose'=>true
//        ]
                        [
                            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                            'language' => 'ru',
                            'value' => date('d/m/y'),
                            'pluginOptions' => [
                                'autoclose'=>true,
//                'format' => 'd/m/yy'
                            ]
                        ]); ?>
                <div class="row">
                    <div class="col-1-5"></div>
                    <div class="col-4-5">
                        <?= Html::submitButton('Вернуть билет', ['class' => 'btn btn-return']) ?>
                    </div>
                </div>
                    <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
        <div class="col-1-4">
        </div>
</div><!-- /.row -->
</div>