<?php

use yii\widgets\ListView;
use voime\GoogleMaps\Map;

/* @var $this \yii\web\view */

$this->title = 'Контакты';

//$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyC-fcAKfCosG70KPap6ZA1en0iurBEylTk&callback=initMap', ['depends' => [yii\web\JqueryAsset::className()]])

?>
<div class="contacts-block">
    <table class="table_info">
        <caption>Контактная информация</caption>
        <tr>
            <td>Адрес</td>
            <td>г.Киев, ул.Дорожницкого,17</td>
        </tr>
        <tr>
            <td>Телефон</td>
            <td>(044)220-55-32</td>
        </tr>
        <tr>
            <td>Мобильный</td>
           <td>095-153-03-33
        <p>096-712-00-41</p></td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td>busstation@gmail.com</td>
        </tr>
    </table>
</div>

<div class="contacts-block">
    <table class="table_info">
        <caption>График работы</caption>
        <tr>
            <td>Офис в Киеве</td>
            <td>Пн-Пт с 9:00 до 17:00</td>
        </tr>
    </table>
</div>


<div class="contacts-block">
    <table class="table_info">
        <caption>Наше расположение</caption>
    </table>
    <?php echo Map::widget([
        'apiKey'=> 'AIzaSyC-fcAKfCosG70KPap6ZA1en0iurBEylTk',
        'zoom' => 16,
        'center' => [50.469901, 30.443973],
        'width' => '80%',
        'height' => '600px',
        'markers' => [
        ['position' => [50.469901,30.443973]]],
        'mapType' => Map::MAP_TYPE_ROADMAP,
    ]); ?>
</div>