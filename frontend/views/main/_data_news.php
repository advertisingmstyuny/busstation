<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.04.2018
 * Time: 22:53
 */

/* @var $this \yii\web\view */
/* @var $model \backend\models\News */

?>
<div class='cbm_wrap '>
        <span class='horiz-flag noise '> <h1 id = "title-news"><?= $model->title ?></h1></span>
            <?php $images = $model->getImages() ?>
            <?php foreach ($images as $image): ?>
                <img src="<?= $image->getUrl(''); ?>" alt="">
            <?php endforeach; ?>
        <p>
            <?= $model->description ?>
        </p>

        <span class='horiz-flag1 noise1 '> <h4 id = 'date'><?= $model->created_at ?></h4></span>
    </div>