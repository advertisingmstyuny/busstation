<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 29.03.2018
 * Time: 14:58
 */

use backend\models\RouteStation;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Schedule */
/* @var $form yii\widgets\ActiveForm*/

$routeStation = RouteStation::findAll(['id_route' => $model->id_route]);

?>

<div class="divTableRow">
    <div class="divTableCell" id="col-width-num">
        <?= $model->route->number ?>
    </div>
        <div class="divTableCell">
            <?= $model->route->title ?>
        </div>

        <div class="divTableCell">
            <?php if ($model->mode === 1): ?>
                по нечетным
            <?php elseif ($model->mode ===2): ?>
                по четным
            <?php else: ?>
                ежедневно
            <?php endif; ?>
        </div>
        <div class="divTableCell" id="col-center">
            <?php $lastStation=count($model->route->routeStations); ?>
            <?php for($i=0; $i<$lastStation; $i++):?>
                <?php if($i==$lastStation-1): ?>
                    <?= $model->route->routeStations[$i]['arrival_time'] ?>
                <?php endif; ?>
            <?php endfor; ?>
        </div>
        <div class="divTableCell" id="col-center">
            <?php foreach ($model->route->routeStations as $routeStation): ?>
                <?= $routeStation->time?>
                <?php break; ?>
            <?php endforeach; ?>
        </div>
        <div class="divTableCell">
<!--            --><?//= Html::a('Подробнее',['/schedule/detail','id' => $model->id],['class' => 'btn btn-details']) ?>
            <div>
                   <?php
                    Modal::begin([
                        'header' =>'<h3>'.$model->route->title.'</h3>',
                        'toggleButton' => [
                            'label' => 'Подробнее',
                            'tag' => 'button',
                            'class' => 'btn btn-details',],
                        'options'=>['class'=> 'modal-detail']
                    ]);
                   ?>
                <div class="divSmallTable sch-small-table">
                    <div class="divSmallTableHeading">
                        <div class="divSmallTableHead">Станции</div>
                        <div class="divSmallTableHead">Время прибытия</div>
                        <div class="divSmallTableHead">Время отправления</div>
                    </div>

                        <div class="divSmallTableBody">
                            <?php foreach ($model->route->routeStations as $rs): ?>

                               <div class="divSmallTableRow">

                                   <div class="divSmallTableCell"> <?= Html::input('checkbox', 'departure', $rs->station->title,['class'=>'limit_checkbox', 'id'=>'checkbox1', 'data-id'=>$model->id])?>

                                       <?= $rs->station->title ?></div>
                                   <div class="divSmallTableCell"><?= $rs->arrival_time?></div>
                                   <div class="divSmallTableCell"><?= $rs->time ?></div>
                               </div>
                            <?php endforeach; ?>
                            <?= DatePicker::widget([
                                'id' => 'date'.$model->id,
                                'name' => 'departure_date',
                                'language' => 'ru',
                                'value' => date('d.m.Y'),
                                'options' => [
                                        'class' => 'date-picker-schedule'
                                ],
                                'pluginOptions' => [
                                        'startDate' => date('d.m.Y')
                                ],
                            ]); ?>
                        </div>
                </div>

                <div>
                    <?= Html::a('Купить билет',['/schedule/schedule-view','id' => $model->id_route],['class' => 'btn btn-save', 'id' => 'btn-buy'.$model->id, 'data-id' => $model->id]) ?>
                </div>
                <?php Modal::end(); ?>
            </div>
        </div>

</div>