<?php

use yii\widgets\ListView;
use yii\helpers\Html;

/* @var $this \yii\web\view */
/* @var $model \backend\models\News */

$this->title = 'Новости';
?>

​<div class='wrapper_body'>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '',
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model) {
            return $this->render('_data_news',[
                'model' => $model
            ]);
        },
    ]) ?>
</div>
