<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01.03.2018
 * Time: 0:49
 */

use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\bootstrap\Carousel;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Schedule */
$this->title = 'Главная';

?>

<div class="string">
    <div id="marquee">

    </div>
</div>

<div class="carouselmain">
    <?php
    echo Carousel::widget([
        'items' => [
            // the item contains only the image
         ['content' =>  Html::img('@web/image/bustourist.jpg',  ['class'=>'img-carousel']),
           'caption' => '<div class="slideUp">
				<ul id="slide-text" class="back-text">
                    <h1>Путешествуй вместе с нами</h1>
					<li>более 300 направлений</li>
					<li>комфортные автобусы</li>
					<li>круглосуточная служба поддержки</li>
				</ul>
				</div>'
         ],
        // equivalent to the above
            [
                 'content' => Html::img('@web/image/oportunity.jpg',  ['class'=>'img-carousel']),
                 'caption' => '<div class="slideUp">
				 <ul id="slide-text" class="back-text">
                    <h1>Актуальные возможности</h1>
					<li>покупка билетов</li>
					<li>возврат билетов</li>
					<li>оповещения о рейсовых изменениях</li>
				</ul>
				</div>',
            ],
            // the item contains both the image and the caption

            [
                'content' => Html::img('@web/image/oplata.jpg',  ['class'=>'img-carousel']),
                'caption' => '<div class="slideUp">
				<ul id="slide-text" class="back-text">
                    <h1>Универсальная оплата</h1>
					<li>платежные карты</li>
					<li>элеткронные деньги</li>
					<li>Приват24</li>
				</ul>
				</div>',
            ],
//            [
//                'content' => Html::img('@web/image/insidepng.png',  ['class'=>'img-carousel'])
//                // 'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
//            ],
        ],
        'options' => ['class' => 'carousel slide', 'data-interval' => '5000'],
        'controls' => [
            '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>',
            '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'
        ]
    ]);
    ?>
</div>

<div class="titul-form">
    <h3>Поиск билетов на автобус</h3>
</div>


<?php $form = ActiveForm::begin([
    'action' => ['schedule/search'],
]); ?>
<div class="autocomplete-block">

   <div class="autocomplete-general">
        <div class="autocomplete-input1">
        <span id="text-form">Откуда</span>
    <?= AutoComplete::widget([
//    'model' => 'title',
//    'attribute' => 'title',
        'name' => 'departure',
        'id' => '1',
        'options' => ['class' => 'form-control'],
        'clientOptions' => [
            'source' => Url::to(['schedule/route']),
            'minLength'=>'1',
            'limit' => '1'
        ],
    ]); ?>
        </div>
       <div class="autocomplete-general">
       <div class="autocomplete-transfer">
            <span class="glyphicon glyphicon-transfer"></span>
       </div>
       </div>
        <div class="autocomplete-input2">
        <span id="text-form">Куда</span>
        <?= AutoComplete::widget([
//    'model' => $model,
            'name' => 'arrival',
            'id' => '2',
            'attribute' => 'title',
            'options' => ['class' => 'form-control'],
            'clientOptions' => [
                'source' => Url::to(['schedule/routet']),
                'minLength'=>'1',
            ],
        ]); ?>
        </div>
   </div>
    <div class="autocomplete-general">
     <div class="autocomplete-input3">
         <span id="text-form">Дата</span>
        <?= DatePicker::widget([
            'name' => 'departure_date',
            'language' => 'ru',
            'value' => date('d.m.Y'),
            'pluginOptions' => [
                'startDate' => date('d.m.Y')
            ]
        ]); ?>
    </div>
    <div class="form-group-btn">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-save']) ?>
    </div>
</div>
</div>
<?php ActiveForm::end(); ?>


<div class="container clearfix">
    <div class="block-buy">
        <h3>Покупка онлайн</h3>
        <div class="block2-buy">
            <div class="image-info">
                    <?= Html::img('@web/image/infobus.jpg') ?>
             </div>
            <div class="text-info">
        Нужно купить автобусные билеты? Busstation.ua без проблем поможет в поиске билета. На сайте Вы найдете расписание автобусов по Украине, также узнаете есть ли свободные места и стоимость билетов, забыв об очередях в автобусную кассу. На Busstation.ua купить билеты легко и просто: необходимо выбрать нужное автобусное направление и указать дату в календаре. Система выдаст подходящие варианты автобусных рейсов по Вашему запросу. Далее выбираете самый удобный и дешевый вариант и покупаете билет на сайте. Busstation.ua сэкономит Ваше время и поможет приобрести билеты на автобус по выгодной цене.
        </div>
        </div>
    </div>
    <div class="block-return">
    <h3>Возврат онлайн</h3>
    <div class="block2-return">
        <div class="image-info">
            <?= Html::img('@web/image/returnTick.jpg') ?>
        </div>
        <div class="text-info">
        На сайте Busstation.ua не только легко купить билет на автобус онлайн, но и легко его вернуть. Возврат билета осуществляется согласно государственному постановлению КМУ «Правила предоставления услуг пассажирского автомобильного транспорта».
        Вернуть неиспользованные автобусные билеты вы можете, заполнив форму "Возврат билета".
        Средства за неиспользованный билет вернутся вам в течение 30 дней. Условия возврата денежных средств варьируются в зависимости от времени до и после отправления автобуса.
    </div>
    </div>
    </div>
</div>