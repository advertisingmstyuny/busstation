<?php

use yii\widgets\ListView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Schedule */
/* @var $sort int */

$this->title = 'Расписание';
?>
<?php Pjax::begin()?>

<div class="divTable sch-table">
    <div class="divTableHeading">
        <div class="divTableRowHead">
            <div class="divTableHead" id="col-width-num1">
                №
                <?php if ($sort == null || $sort == 2 || $sort == 3 || $sort == 4 || $sort == 5 || $sort == 6): ?>
                    <?= Html::a('<span class="glyphicon glyphicon-chevron-up"></span>', ['/main/schedule', 'sort' => 1], ['class' => 'sort-number-asc','data-pjax' => true]); ?>
                <?php elseif ($sort == 1): ?>
                    <?= Html::a('<span class="glyphicon glyphicon-chevron-down"></span>', ['/main/schedule', 'sort' => 2], ['class' => 'sort-number-desc','data-pjax' => true]); ?>
                <?php endif; ?>
            </div>
            <div class="divTableHead">Рейс</div>

<!--            <div class="divTableHead">Маршрут</div>-->
            <div class="divTableHead" >График</div>
            <div class="divTableHead" id="col-center">
                Время отправления
                <?php if ($sort == null || $sort == 1 || $sort == 2 || $sort == 4 || $sort == 5 || $sort == 6): ?>
                    <?= Html::a('<span class="glyphicon glyphicon-chevron-up"></span>', ['/main/schedule', 'sort' => 3], ['class' => 'sort-number-asc','data-pjax' => true]); ?>
                <?php elseif ($sort == 3): ?>
                    <?= Html::a('<span class="glyphicon glyphicon-chevron-down"></span>', ['/main/schedule', 'sort' => 4], ['class' => 'sort-number-desc','data-pjax' => true]); ?>
                <?php endif; ?>
            </div>
            <div class="divTableHead" id="col-center">
                Время прибытия
                <?php if ($sort == null || $sort == 1 || $sort == 2 || $sort == 3 || $sort == 4 || $sort == 6): ?>
                    <?= Html::a('<span class="glyphicon glyphicon-chevron-up"></span>', ['/main/schedule', 'sort' => 5], ['class' => 'sort-number-asc','data-pjax' => true]); ?>
                <?php elseif ($sort == 5): ?>
                    <?= Html::a('<span class="glyphicon glyphicon-chevron-down"></span>', ['/main/schedule', 'sort' => 6], ['class' => 'sort-number-desc','data-pjax' => true]); ?>
                <?php endif; ?>
            </div>
            <div class="divTableHead" > </div>
        </div>
    </div>
    <div class="divTableBody">
        <?= ListView::widget([
                'dataProvider' => $dataProvider,
//                'sorter' => ['attributes'=>'number'],
                'summary' => '',
                'options' => [
                    'data-pjax' => true
                ],
                'itemOptions' => [
                    'class' => 'item',
                ],
                'itemView' => function ($model) {
                    return $this->render('_data_schedule',[
                        'model' => $model
                    ]);
                },
            ]) ?>
    </div>
</div>

<?php Pjax::end(); ?>

