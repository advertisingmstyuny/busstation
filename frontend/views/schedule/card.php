<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\Driver */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card-form">
        <h3 align="center">Чтобы оплатить, заполните, пожалуйста, необходимые поля</h3>

    <?php $form = ActiveForm::begin(); ?>

            <div class="card-form-in">
    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number_card')->textInput(['maxlength' => true]) ?>
    <?= Html::hiddenInput('id', $id) ?>
                <?= Html::hiddenInput('departure', $departure) ?>
                <?= Html::hiddenInput('arrival', $arrival) ?>
                <?= Html::hiddenInput('place', $place) ?>
                <?= Html::hiddenInput( 'price', $price) ?>
                <?= Html::hiddenInput('date', $date) ?>
                <?= Html::hiddenInput( 'customer', $customer) ?>

    <div class="form-group">
        <?= Html::submitButton('Оплатить', ['class' => 'btn btn-purchase']) ?>
    </div>


    <?php ActiveForm::end(); ?>
</div>
</div>