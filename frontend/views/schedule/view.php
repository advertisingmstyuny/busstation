<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 29.03.2018
 * Time: 16:10
 */

use yii\bootstrap\Html;
use yii\bootstrap\Modal;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Schedule */
/* @var $departure string */
/* @var $arrival string */
/* @var $date string */
/* @var $model \backend\models\Profile */


$this->title = ' '.$departure.' - '.$arrival;

$array = array();

foreach ($model->orders as $order) {
    if ($order->departure_date == $date) {
        $array_ex = explode(';', $order->place);
        for ($i = 0; $i < count($array_ex); $i++) {
            array_push($array, $array_ex[$i]);
        }
    }
}
?>
<?php if (Yii::$app->session->hasFlash('errorPlace')): ?>

    <div class="alert alert-danger">
        <?= Yii::$app->session->getFlash('errorPlace') ?>
    </div>

<?php endif; ?>
<div class="schedule-view">

    <div class="divSmallTable sch-small-table">
        <div class="divSmallTableHeading">
            <div class="divSmallTableHead">№</div>
            <div class="divSmallTableHead">Рейс</div>
            <div class="divSmallTableHead">Место отправления</div>
            <div class="divSmallTableHead">Место прибытия</div>
            <div class="divSmallTableHead">Дата и время отправления</div>
            <div class="divSmallTableHead">Цена за билет</div>
        </div>

        <div class="divSmallTableBody">
            <div class="divSmallTableRow">
                <div class="divSmallTableCell"><?= $model->route->number ?></div>
                <div class="divSmallTableCell"><?= $model->route->title ?></div>
                <div class="divSmallTableCell"><?= $departure ?></div>
                <div class="divSmallTableCell"><?= $arrival ?></div>
                <div class="divSmallTableCell"><?= $date ?> |
                    <?php foreach ($model->route->routeStations as $rs): ?>
                        <?php if ($rs->station->title == $departure): ?>
                            <?= $rs->time ?>
                        <?php endif; ?>
                    <?php endforeach; ?></div>
                <div class="divSmallTableCell"> <?= $price ?>грн</div>
            </div>
        </div>
    </div>


<div class="select-places">
    <div class ="block-places">
    <?php for($i = 0; $i < $model->vehicleDriver->vehicle->count_places; $i++): ?>
        <?php if (in_array($i+1, $array) == true): ?>
            <div class="placess" id="<?= $model->id ?>" data-id="<?= $i+1 ?>">
                <p><?= $i+1 ?></p>
            </div>
        <?php endif; ?>
        <?php if (in_array($i+1, $array) == false ): ?>
            <div class="places" id="<?= $model->id ?>" data-id="<?= $i+1 ?>">
                <p><?= $i+1 ?></p>
            </div>
        <?php endif; ?>
    <?php endfor; ?>
</div>
    <div class="driver-place">
        <div id="text-dr">Водитель</div>
    </div>
</div>

<?//= Html::a('Оформить заказ',
//    [   '/schedule/order',
//        'id' => $model->id,
//        'departure' => $departure,
//        'arrival' => $arrival,
//        'price' => $totalPrice,
//        'date' => $date
//    ],['class' => 'btn btn-order'] ) ?>



<div>
    <?php
    Modal::begin([
            'header' => '<h4>Выберите способ оплаты</h4>',
            'toggleButton' => [

            'label' => 'Купить',
            'tag' => 'button',
            'data-id' => $model->id,
            'data-departure' => $departure,
            'data-arrival' => $arrival,
            'data-price' => $price,
            'data-date' => $date,
            'class' => 'btn btn-order',
                'id' => 'btn-order'
                 ],
        'id' => 'modal-order'

    ]);
    ?>

    <?php Modal::end(); ?>
</div>

</div>