<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13.03.2018
 * Time: 12:53
 */

use backend\models\Route;
use backend\models\RouteStation;
use yii\bootstrap\Html;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Schedule */

//var_dump($model);die();

$route = Route::findOne(['id' => $model['id_route']]);
$routeStation = RouteStation::findAll(['id_route' => $model['id_route']]);
?>

<div class="search-data-view">

    <div class="search-data-info" id="col-width-numb">
        <?= $route->number ?>
    </div>
    <div class="search-data-info">
        <?= $route->title ?>
    </div>
    <div class="search-data-info">
        <?php foreach ($routeStation as $rs): ?>
            <?php if ($rs->station['title'] == $departure_c || $rs->station['title'] == $arrival_c): ?>
                <b><?= $rs->station['title'] ?></b>
            <?php else: ?>
                <?= $rs->station['title'] ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <div class="search-data-info" id="col-centr">
        <?php foreach ($routeStation as $rs): ?>
          <?php if ($rs->station['title'] == $departure_c): ?>
                <?= $rs->time ?>
          <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <div class="search-data-info" id="col-centr">
        <?php foreach ($routeStation as $rs): ?>
            <?php if ($rs->station['title'] == $arrival_c): ?>
                <?= $rs->arrival_time ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <div class="search-data-info">
        <?= Html::a('Купить билет' ,
            ['/schedule/view', 'id' => $model['id_route'],'departure' => $departure_c,'arrival' => $arrival_c, 'date' => $departure_date],
            ['class' => 'btn btn-buy-ticket']) ?>
    </div>

</div>