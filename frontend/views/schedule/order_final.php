<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.04.2018
 * Time: 20:58
 */
use yii\helpers\Html;

/* @var $this \yii\web\view */
/* @var $model \backend\models\OrderProfile */
/* @var $price double */

?>
<p>К оплате: <?= $price ?> грн</p>
<div class="purchase">
    <?= Html::a('Оплатить картой', ['/schedule/buy', 'id' => $id, 'departure' => $departure, 'arrival' => $arrival, 'place' => $place, 'price' => $price, 'date' => $date, 'customer' => $customer], ['class' => 'btn btn-purchase ']); ?>
    <?= Html::a('Оплатить на кассе', ['/schedule/success', 'id' => $id, 'departure' => $departure, 'arrival' => $arrival, 'place' => $place, 'price' => $price, 'date' => $date, 'customer' => $customer], ['class' => 'btn btn-purchase']); ?>
</div>
