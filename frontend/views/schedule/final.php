<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 02.04.2018
 * Time: 14:33
 */
use yii\bootstrap\Html;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Order */
/* @var $price double */

?>

<p>К оплате: <?= $price ?> грн</p>
<div class="purchase">
    <?= Html::a('Оплатить картой', ['/schedule/buy', 'id' => $id, 'departure' => $departure, 'arrival' => $arrival, 'place' => $place, 'price' => $price, 'date' => $date, 'customer' => $customer], ['class' => 'btn btn-purchase ']); ?>
    <?= Html::a('Оплатить на кассе', ['/schedule/success', 'id' => $id, 'departure' => $departure, 'arrival' => $arrival, 'place' => $place, 'price' => $price, 'date' => $date, 'customer' => $customer], ['class' => 'btn btn-purchase']); ?>
</div>