<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 13.03.2018
 * Time: 12:51
 */

use yii\widgets\ListView;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Schedule */
/* @var $dataProvider string */

$this->title = 'Результаты поиска';
?>

<div class="divTableS search-table">
    <div class="search-head">
        <div class="search-row-head">
            <div class="search-head-info" id="col-width-num-head">№</div>
            <div class="search-head-info">Рейс</div>
            <div class="search-head-info">Маршрут</div>
            <div class="search-head-info" id="col-centr">Время отправления</div>
            <div class="search-head-info" id="col-centr" >Время прибытия</div>
            <div class="search-head-info"> </div>
        </div>
    </div>

    <div class="search-body">
        <?php echo ListView::widget([
            'id' => 'post',
            'dataProvider' => $dataProvider,
            'summary' => '',
            'options' => [
                'data' => ['pjax' => true]
            ],
            'itemView' => '_data',
            'viewParams' => [
                'fullView' => true,
                'context' => 'main-page',
                'departure_c' => $departure_c,
                'arrival_c' => $arrival_c,
                'departure_date' => $departure_date
            ]
        ]) ?>
    </div>

</div>
