<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 02.04.2018
 * Time: 14:24
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\Customer */
/* @var $form yii\widgets\ActiveForm */

//$this->registerJsFile(Yii::getAlias('@web').'/js/main.js',['depends' => [yii\web\JqueryAsset::className()]]);
?>

<div class="customer-form">
    <?php Pjax::begin() ?>

    <?php $form = ActiveForm::begin([
//            'action' => '/schedule/order',
            'options' => ['data-pjax'=> true]
    ]); ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <?= Html::hiddenInput('price', $price) ?>
    <?= Html::hiddenInput('id', $id) ?>
    <?= Html::hiddenInput('departure', $departure) ?>
    <?= Html::hiddenInput('arrival', $arrival) ?>
    <?= Html::hiddenInput('date', $date) ?>
    <?= Html::hiddenInput('place', $place) ?>

    <div class="form-group">
        <?= Html::submitButton('Cохранить', [
                'class' => 'btn btn-success btn-cust'
        ]) ?>
<!--        --><?//= Html::button('Дальше', ['class' =>'btn btn-success btn-sub'])?>
    </div>


    <?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>
</div>