<?php
/**
 * @var $this \yii\web\view
 * @var $model \backend\models\Order
 */
?>
<div class="ticket-blank">
    <?php if(Yii::$app->user->isGuest):?>
        <p><?= $model->customer->full_name ?></p>
    <?php else: ?>
        <p><?=  $model->orderProfiles->profile->first_name ?><?=  $model->orderProfiles->profile->second_name ?></p>
    <?php endif; ?>
    <p><?= $model->id ?></p>
    <p><?=  $model->place ?></p>
    <p><?=  $model->end_price ?></p>
    <p><?=  $model->station_departure ?></p>
    <p><?=  $model->station_arrival ?></p>
    <?php if($model->status==0): ?>
    <p>Неоплачено</p>
    <?php else: ?>
    <p>Оплачено</p>
    <?php endif; ?>
</div>




