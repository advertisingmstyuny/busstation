<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01.03.2018
 * Time: 0:50
 */

use frontend\assets\AppAsset;
use yii\bootstrap\Html;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use common\widgets\Alert;

/* @var $content string */
/* @var $this \yii\web\view */

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <header class="navbar-fixed-top">
            <div class="navigation">

                <?php NavBar::begin([
                    'brandLabel' => Html::img('@web/image/logo.png', ['class'=>'navbrand'] ) ,
                    'brandUrl' => Yii::$app->homeUrl,

                    'options' => [
                        'id' => 'navbar-main',
                        'class' => 'background-menu-color navbar _navbar-inverse navbar-default'
                             ],
                ]);
                echo Nav::widget([
                    'options' => [
                        'class' => 'navbar-nav'
                    ],
                    'items' => [
                        ['label' => 'Главная', 'url' => ['/'], 'options' => ['class' => 'under-line-menu'] ],
                        ['label' => 'Расписание', 'url' => ['/main/schedule'], 'options' => ['class' => 'under-line-menu']],
                        ['label' => 'Возврат билета', 'url' => ['/main/return'], 'options' => ['class' => 'under-line-menu']],
                        ['label' => 'Новости', 'url' => ['/main/news'], 'options' => ['class' => 'under-line-menu']],
                        ['label' => 'Контакты', 'url' => ['/main/contacts'], 'options' => ['class' => 'under-line-menu']],

                    ],
                ]);

                if (Yii::$app->user->isGuest) {
                    $navView[]=[
                        'label' => 'Войти', 'url' => ['/main/login'], 'options' => ['class' => 'under-line-menu']
                    ];

                }
                else {
                    $navView[]=[
                        'label' => '<span class="glyphicon glyphicon-user"></span>',
                        'items' => [
                                '<li class="dropdown-header">'.Yii::$app->user->identity['username'].'</li>',
                                '<li class="divider"></li>',
                            [
                                'label' => 'Профиль',
                                'url' => ['/profile/view','id' => Yii::$app->user->identity['id']],
                                'linkOptions' => ['data-method' => 'post']
                            ],
                            [
                                'label' => 'Выход',
                                'url' => ['/main/logout'],
                                'linkOptions' => ['data-method' => 'post']
                            ],
                        ]
                    ];
                }
                echo Nav::widget([
                    'options' => [
                        'class' => 'navbar-nav navbar-right'
                    ],
                    'encodeLabels' => false,
                    'items' => $navView

                ]);
                NavBar::end();?>
            </div>
        </header>
        <div class="page-view"></div>
            <?= Alert::widget() ?>
            <?= $content ?>
    </div>



    <footer class="footer">
        <div class="container">
            <div class="logo-ch-block logo-contact">
                <p><b>Для связи с нами</b></p>
                <div class="phone">
                    <span class="glyphicon glyphicon-earphone"></span> <b><i>(044)220-55-32</i></b>
                </div>
                <div class="mail">
                    <span class="glyphicon glyphicon-envelope"></span> <b><i>busstation@gmail.com</i></b>
                </div>
            </div>
<!--            <p class="pull-left">&copy; --><?//= Html::encode(Yii::$app->name) ?><!-- --><?//= date('Y') ?><!--</p>-->

<!--            <p class="pull-right">--><?//= Yii::powered() ?><!--</p>-->
        </div>
    </footer>


    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>