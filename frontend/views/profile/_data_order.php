<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.04.2018
 * Time: 21:47
 */

/* @var $this \yii\web\view */
/* @var $model \backend\models\OrderProfile */

?>
<!--<div class="order-profile">-->
    <div class="divPro-HTable profile-history-table">
        <div class="divHTableHeading">
            <div class="divHTableHead">№</div>
            <div class="divHTableHead">Рейс</div>
            <div class="divHTableHead">Место отправления</div>
            <div class="divHTableHead">Место прибытия</div>
            <div class="divHTableHead">№ места</div>
            <div class="divHTableHead">Дата покупки билета</div>
            <div class="divHTableHead">Дата отправления</div>
            <div class="divHTableHead">Cумма</div>
            <div class="divHTableHead">Статус</div>
        </div>

        <div class="divHTableBody">
                <div class="divHTableRow">
                    <div class="divHTableCell"><?= $model->order->schedule->route->number ?></div>
                    <div class="divHTableCell"><?= $model->order->schedule->route->title ?></div>
                    <div class="divHTableCell"><?= $model->order->station_departure ?></div>
                    <div class="divHTableCell"><?= $model->order->station_arrival ?></div>
                    <div class="divHTableCell"><?= $model->order->place ?></div>
                    <div class="divHTableCell"><?= $model->order->create_at ?></div>
                    <div class="divHTableCell"><?= $model->order->departure_date ?></div>
                    <div class="divHTableCell"><?= $model->order->end_price ?> грн</div>
                    <div class="divHTableCell">
                        <?php if ($model->status == 1): ?>
                            Билет действительный
                        <?php elseif ($model->status == 2) : ?>
                            Вы уже использовали этот билет
                        <?php endif; ?>
                    </div>
                </div>
        </div>
    </div>

<!--</div>-->
