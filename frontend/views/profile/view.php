<?php

use yii\widgets\ListView;
use yii\helpers\Html;
/* @var $this \yii\web\view */
/* @var $model \frontend\models\Profile */
/* @var $dataProvider string */

$this->title = $model->first_name . ' ' . $model->second_name;

?>
<div class="container">
    <div class="profile-block">
        <div class="profile-head">
            <h2><?= $model->first_name ?> <?= $model->third_name ?> <?= $model->second_name ?> </h2>
        </div>
        <div class="profile-body">
            <p><span class="profile-body-text">Пол:
                    <?php if ($model->sex === 2): ?>
                        мужской
                    <?php else: ?>
                        женский
                    <?php endif; ?>
            </span></p>
            <p><span class="profile-body-text">Дата рождения: <?= $model->birthday ?></span></p>
        </div>
        <div class="profile-footer">
           <?= Html::a('Редактировать', ['/profile/edit', 'id'=>Yii::$app->user->identity['id']], ['class'=>'btn btn-info']) ?>
            <button class="btn btn-info">История поездок</button>
        </div>
    </div>

    <div class="profile-order-block">

        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model) {
                return $this->render('_data_order',[
                    'model' => $model
                ]);
            },
        ]) ?>
    </div>
</div>
