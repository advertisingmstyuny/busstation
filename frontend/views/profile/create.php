<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\date\DatePicker;
/* @var $this \yii\web\view */
/* @var $model \frontend\models\Profile */
/* @var $form \yii\widgets\ActiveForm */
?>

<div class="form-create-profile">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'second_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'third_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sex')->dropDownList([
            '1'=>'женский',
            '2'=>'мужской'
    ]) ?>

    <?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
        'type'=> DatePicker::TYPE_INLINE,
        'options' => ['placeholder' => 'Введите день своего рождения ...'],
        'pluginOptions' => [
            'autoclose'=>true
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
