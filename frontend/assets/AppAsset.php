<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/navbar.css',
        'css/frontend.css',
        'css/profile.css',
        'css/schedule.css',
        'css/search.css',
        'css/contacts.css',
        'css/news.css',
        'css/return.css',
        'css/login.css'

    ];
    public $js = [
        'js/main.js',
//        'https://maps.googleapis.com/maps/api/js?key=AIzaSyC-fcAKfCosG70KPap6ZA1en0iurBEylTk&callback=initMap'
    ];
    public $jsOptions = [
//        'position' => \yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
