<?php

namespace frontend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "profile".
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $second_name
 * @property int $sex
 * @property string $birthday
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sex'], 'integer'],
            [['first_name', 'second_name', 'sex', 'birthday'], 'required'],
            [['first_name', 'second_name', 'birthday'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'user_id' => 'Пользователя ID',
            'first_name' => 'Имя',
            'second_name' => 'Фамилия',
            'sex' => 'Пол',
            'birthday' => 'Дата рождения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

//    public function profile()
//    {
//        if(!$this->validate()){
//            return null;
//            }
//        $profile=new Profile();
//        $profile->user_id=Yii::$app->user->id;
//        $profile->first_name=$this->first_name;
//        $profile->second_name=$this->second_name;
//        $profile->sex=$this->sex;
//        $profile->birthday=$this->birthday;
//        return $profile->save() ? $profile : null;
//    }
}
