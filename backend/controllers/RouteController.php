<?php

namespace backend\controllers;

use backend\models\RouteStation;
use backend\models\Station;
use Yii;
use backend\models\Route;
use backend\models\RouteSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * RouteController implements the CRUD actions for Route model.
 */
class RouteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'create-station', 'station', 'add-station', 'delete-station', 'view-station'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Route models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RouteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Route model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Route model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Route();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['create-station', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateStation($id)
    {
        $model = new RouteStation();

        $dataProvider = new ActiveDataProvider([
            'query' => RouteStation::find()->where(['id_route' => $id])
        ]);

        return $this->render('create-route',[
            'model' => $model,
            'dataProvider' => $dataProvider,
            'id' => $id
        ]);
    }

    public function actionStation($id)
    {
        $model = new Station();

        if ($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->goBack(['/route/create-station','id' => $id]);
        }

        return $this->render('create_station',[
            'model' => $model
        ]);
    }

    public function actionAddStation()
    {
        $route_id = Yii::$app->request->post('route_id');
        $station = Yii::$app->request->post('station');
        $length = Yii::$app->request->post('length');
        $time = Yii::$app->request->post('time');
        $arrivalTime = Yii::$app->request->post('arrival_time');

        $station = Station::findOne(['title' => $station]);

        $station_id = $station->id;

//        if (RouteStation::findOne(['id_route' => $route_id,'id_station' => $station_id]) != true) {
//            if ($station_id != null || $station_id != 0){
                $model = new RouteStation();

                $model->id_route = $route_id;
                $model->id_station = $station_id;
                $model->length = $length;
                $model->time = $time;
                $model->arrival_time = $arrivalTime;
                $model->save();

                return $this->renderAjax('_data_station', [
                    'model' => $model,
                ]);
//            }
//        }

//        return 'Такое уже есть';
    }

    public function actionsDeleteStation()
    {
        $id = Yii::$app->request->post('id');
        $this->findModelStation($id)->delete();
    }

    public function actionViewStation()
    {
        $qq = Yii::$app->request->get('term');
//        $qq = 'Зап';
        $query = Station::find()->select(['title as value','title as label'])->where(['like','title', $qq])
            ->asArray('title')
            ->all();
        $result = array();
//          $qq = 'Зап';
        foreach($query as $q){
//            $value = explode('-', $q['value']);
            if (strstr($q['value'], $qq)) {

                $result[] = array('value' => $q['value']);
                break;
            }
        }
        return json_encode($result);
    }

    /**
     * Updates an existing Route model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Route model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Route model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Route the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Route::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the RouteStation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RouteStation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelStation($id)
    {
        if (($model = RouteStation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
