<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.03.2018
 * Time: 15:23
 */

namespace backend\controllers;


use yii\web\Controller;
use Yii;
use yii\filters\AccessControl;

class NewsController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['view', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $getNews = Yii::$app->request->post('news');

        $filename = 'news.txt';
        $data = file_get_contents($filename);
        $news = unserialize($data);

        if ($getNews != null){
            if ($news == null) {
                $news = array();
            }
            array_push($news,['news' => $getNews]);
            $data = serialize($news);
            file_put_contents($filename,$data);
            return $this->redirect(['view']);
        }
        return $this->render('create');
    }

    public function actionView()
    {
        $data = file_get_contents('news.txt');
        $news = unserialize($data);

        return $this->render('view', [
            'news' => $news
        ]);
    }

    public function actionDelete()
    {
        $elem = Yii::$app->request->post('elem');

        $data = file_get_contents('news.txt');
        $news = unserialize($data);

        $this->deleteArray($news, $elem);


        $data = serialize($news);
        file_put_contents('news.txt',$data);

        return json_encode($news);
    }

    private function deleteArray(&$array, $value){
        foreach( $array as $key => $val ){
            if( is_array($val) ){
                $this->deleteArray($array[$key], $value);
            }elseif( $val===$value ){
                unset($array[$key]);
            }
        }
    }

}