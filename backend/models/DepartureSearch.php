<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Departure;

/**
 * DepartureSearch represents the model behind the search form of `backend\models\Departure`.
 */
class DepartureSearch extends Departure
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_vehicle_driver', 'id_schedule'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Departure::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_vehicle_driver' => $this->id_vehicle_driver,
            'id_schedule' => $this->id_schedule,
        ]);

        return $dataProvider;
    }
}
