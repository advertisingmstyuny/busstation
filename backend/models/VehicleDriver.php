<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vehicle_driver".
 *
 * @property int $id
 * @property int $id_driver
 * @property int $id_vehicle
 *
 * @property Departure[] $departures
 * @property Schedule[] $schedules
 * @property Driver $driver
 * @property Vehicle $vehicle
 */
class VehicleDriver extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicle_driver';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_driver', 'id_vehicle'], 'integer'],
            [['id_driver'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::className(), 'targetAttribute' => ['id_driver' => 'id']],
            [['id_vehicle'], 'exist', 'skipOnError' => true, 'targetClass' => Vehicle::className(), 'targetAttribute' => ['id_vehicle' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'id_driver' => 'Водитель',
            'id_vehicle' => 'Автобус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartures()
    {
        return $this->hasMany(Departure::className(), ['id_vehicle_driver' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchedules()
    {
        return $this->hasMany(Schedule::className(), ['id_vehicle_driver' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['id' => 'id_driver']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicle()
    {
        return $this->hasOne(Vehicle::className(), ['id' => 'id_vehicle']);
    }
}
