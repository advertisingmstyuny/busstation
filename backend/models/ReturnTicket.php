<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "return_ticket".
 *
 * @property int $id
 * @property int $number_ticket
 * @property string $full_name
 * @property string $number_card
 * @property string $date_pay
 * @property string $created_at
 * @property string $phone
 *
 * @property Order $numberTicket
 */
class ReturnTicket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'return_ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number_ticket', 'full_name', 'number_card', 'date_pay', 'phone'], 'required'],
            [['number_ticket'], 'integer'],
            [['full_name'], 'string', 'max' => 512],
            [['date_pay', 'created_at', 'phone', 'number_card'], 'string', 'max' => 255],
            [['number_ticket'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['number_ticket' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'number_ticket' => 'Номер билета',
            'full_name' => 'ФИО',
            'number_card' => 'Номер карты',
            'date_pay' => 'Дата оплаты',
            'created_at' => 'Дата оформления возврата',
            'phone'=>'Номер телефона'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumberTicket()
    {
        return $this->hasOne(Order::className(), ['id' => 'number_ticket']);
    }
}
