<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "route".
 *
 * @property int $id
 * @property string $title
 * @property double $length
 * @property double $price
 * @property int $number
 * @property RouteStation[] $routeStations
 * @property Schedule[] $schedules
 */
class Route extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'route';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'length', 'price', 'number'], 'required'],
            [['length', 'price', 'number'], 'number'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'title' => 'Название',
            'length' => 'Растояние',
            'price' => 'Цена',
            'number' =>'№ рейса',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRouteStations()
    {
        return $this->hasMany(RouteStation::className(), ['id_route' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchedules()
    {
        return $this->hasMany(Schedule::className(), ['id_route' => 'id']);
    }
}
