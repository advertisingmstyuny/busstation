<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "profile".
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $second_name
 * @property int $sex
 * @property string $birthday
 * @property string $phone_number
 * @property string $third_name
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sex'], 'integer'],
            [['first_name', 'second_name', 'sex', 'birthday', 'third_name'], 'required'],
            [['first_name', 'second_name', 'birthday', 'phone_number', 'third_name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'user_id' => '№ пользователя',
            'first_name' => 'Имя',
            'second_name' => 'Фамилия',
            'sex' => 'Пол',
            'birthday' => 'Дата рождения',
            'phone_number' => 'Телефон',
            'third_name' => 'Отчество'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
