<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "schedule".
 *
 * @property int $id
 * @property int $id_route
 * @property int $id_vehicle_driver
 * @property string $from_date
 * @property string $to_date
 * @property int $mode
 *
 * @property Departure[] $departures
 * @property Order[] $orders
 * @property Route $route
 * @property VehicleDriver $vehicleDriver
 */
class Schedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_route', 'id_vehicle_driver', 'mode'], 'integer'],
            [['from_date', 'to_date'], 'string', 'max' => 255],
            [['id_route'], 'exist', 'skipOnError' => true, 'targetClass' => Route::className(), 'targetAttribute' => ['id_route' => 'id']],
            [['id_vehicle_driver'], 'exist', 'skipOnError' => true, 'targetClass' => VehicleDriver::className(), 'targetAttribute' => ['id_vehicle_driver' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'id_route' => 'Маршрут',
            'id_vehicle_driver' => 'Водитель',
            'mode' => 'Режим работы',
            'from_date' => 'С',
            'to_date' => 'По'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartures()
    {
        return $this->hasMany(Departure::className(), ['id_schedule' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id_schedule' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Route::className(), ['id' => 'id_route']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicleDriver()
    {
        return $this->hasOne(VehicleDriver::className(), ['id' => 'id_vehicle_driver']);
    }
}
