<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "departure".
 *
 * @property int $id
 * @property int $id_vehicle_driver
 * @property int $id_schedule
 *
 * @property Schedule $schedule
 * @property VehicleDriver $vehicleDriver
 */
class Departure extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'departure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_vehicle_driver', 'id_schedule'], 'integer'],
            [['id_schedule'], 'exist', 'skipOnError' => true, 'targetClass' => Schedule::className(), 'targetAttribute' => ['id_schedule' => 'id']],
            [['id_vehicle_driver'], 'exist', 'skipOnError' => true, 'targetClass' => VehicleDriver::className(), 'targetAttribute' => ['id_vehicle_driver' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'id_vehicle_driver' => '№ водителя',
            'id_schedule' => '№ рейса',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchedule()
    {
        return $this->hasOne(Schedule::className(), ['id' => 'id_schedule']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicleDriver()
    {
        return $this->hasOne(VehicleDriver::className(), ['id' => 'id_vehicle_driver']);
    }
}
