<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $id_customer
 * @property int $id_schedule
 * @property string $station_departure
 * @property string $station_arrival
 * @property string $create_at
 * @property string $place
 * @property double $end_price
 * @property string $departure_date
 * @property int $status
 *
 * @property Customer $customer
 * @property Schedule $schedule
 * @property OrderProfile $orderProfiles
 * @property ReturnTicket $returnTicket
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_customer', 'id_schedule', 'status'], 'integer'],
            [['station_departure', 'station_arrival', 'create_at', 'end_price'], 'required'],
            [['end_price'], 'number'],
            [['station_departure', 'station_arrival', 'create_at', 'place', 'departure_date'], 'string', 'max' => 255],
            [['id_customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['id_customer' => 'id']],
            [['id_schedule'], 'exist', 'skipOnError' => true, 'targetClass' => Schedule::className(), 'targetAttribute' => ['id_schedule' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'id_customer' => '№ покупателя',
            'id_schedule' => '№ рейса',
            'station_departure' => 'Станция отправления',
            'station_arrival' => 'Станция прибытия',
            'create_at' => 'Дата создания',
            'place' => 'Место',
            'end_price' => 'Конечная цена',
            'departure_date'=>'Дата отправления',
            'status' => 'Статус'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'id_customer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchedule()
    {
        return $this->hasOne(Schedule::className(), ['id' => 'id_schedule']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProfiles()
    {
        return $this->hasOne(OrderProfile::className(), ['id_order' => 'id']);
    }
    public function getReturnTicket()
    {
        return $this->hasOne(ReturnTicket::className(), ['number_ticket' => 'id']);
    }
}
