<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "station".
 *
 * @property int $id
 * @property string $title
 *
 * @property RouteStation[] $routeStations
 */
class Station extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'station';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'title' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRouteStations()
    {
        return $this->hasMany(RouteStation::className(), ['id_station' => 'id']);
    }
}
