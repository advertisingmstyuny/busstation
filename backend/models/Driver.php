<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "driver".
 *
 * @property int $id
 * @property string $full_name
 * @property string $phone_number
 * @property string $address
 * @property string $create_at
 *
 * @property VehicleDriver[] $vehicleDrivers
 */
class Driver extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'phone_number', 'address', 'create_at'], 'required'],
            [['full_name'], 'string', 'max' => 512],
            [['phone_number', 'create_at'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'full_name' => 'ФИО',
            'phone_number' => 'Номер телефона',
            'address' => 'Домашний адрес',
            'create_at' => 'Дата добавления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicleDrivers()
    {
        return $this->hasMany(VehicleDriver::className(), ['id_driver' => 'id']);
    }
}
