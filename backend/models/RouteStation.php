<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "route_station".
 *
 * @property int $id
 * @property int $id_route
 * @property int $id_station
 * @property int $length
 * @property string $time
 * @property string $arrival_time
 *
 * @property Route $route
 * @property Station $station
 */
class RouteStation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'route_station';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_route', 'id_station', 'length'], 'integer'],
            [['time', 'arrival_time'],'safe'],
            [['id_route'], 'exist', 'skipOnError' => true, 'targetClass' => Route::className(), 'targetAttribute' => ['id_route' => 'id']],
            [['id_station'], 'exist', 'skipOnError' => true, 'targetClass' => Station::className(), 'targetAttribute' => ['id_station' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'id_route' => '№ маршрута',
            'id_station' => '№ станции',
            'length' => 'Растояние',
            'time' => 'Время отправления',
            'arrival_time' => 'Время прибытия'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Route::className(), ['id' => 'id_route']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStation()
    {
        return $this->hasOne(Station::className(), ['id' => 'id_station']);
    }
}
