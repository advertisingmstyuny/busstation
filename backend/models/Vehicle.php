<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vehicle".
 *
 * @property int $id
 * @property string $vehicle_number
 * @property string $vehicle
 * @property int $count_places
 * @property string $model
 *
 * @property VehicleDriver[] $vehicleDrivers
 */
class Vehicle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vehicle_number', 'vehicle', 'count_places', 'model'], 'required'],
            [['count_places'], 'integer'],
            [['vehicle_number', 'vehicle', 'model'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'vehicle_number' => 'Номер автобуса',
            'vehicle' => 'Автобус',
            'count_places' => 'Количество мест',
            'model' => 'Модель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicleDrivers()
    {
        return $this->hasMany(VehicleDriver::className(), ['id_vehicle' => 'id']);
    }
}
