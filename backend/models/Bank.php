<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "bank".
 *
 * @property int $id
 * @property string $full_name
 * @property int $number_card
 * @property double $money
 * @property string $created_at
 */
class Bank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'number_card'], 'required'],
            [['number_card'], 'integer'],
            [['money'], 'number'],
            [['full_name', 'created_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'full_name' => 'Фамилия и имя',
            'number_card' => 'Номер карты',
            'money' => 'Сумма',
            'created_at' => 'Дата создания',
        ];
    }
}
