<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Route;
use backend\models\VehicleDriver;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\Schedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_route')->dropDownList(ArrayHelper::map(Route::find()->all(), 'id', 'title'),[
        'prompt' => 'Маршрут',
    ]) ?>

<!--    --><?//= $form->field($model, 'id_vehicle_driver')->textInput() ?>

    <?= $form->field($model, 'id_vehicle_driver')->dropDownList(ArrayHelper::map(VehicleDriver::find()->all(), 'id', 'driver.full_name'),[
        'prompt' => 'Водитель',
    ]) ?>

    <?= $form->field($model, 'mode')->dropDownList([
        '0' => 'ежедневно',
        '1' => 'нечетные',
        '2' => 'четные']); ?>

    <?= $form->field($model,'from_date')->widget(DatePicker::classname(), [
        'language' => 'ru',
        'options' => ['placeholder' => 'Введите дату ...'],
        'pluginOptions' => [
            'autoclose'=>true
        ]
    ]) ?>

    <?= $form->field($model,'to_date')->widget(DatePicker::classname(), [
        'language' => 'ru',
        'options' => ['placeholder' => 'Введите дату ...'],
        'pluginOptions' => [
            'autoclose'=>true
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
