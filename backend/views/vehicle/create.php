<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Vehicle */

$this->title = 'Добавление автобуса';
$this->params['breadcrumbs'][] = ['label' => 'Автобусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
