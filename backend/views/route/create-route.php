<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.03.2018
 * Time: 23:09
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this \yii\web\view */
/* @var $model \backend\models\RouteStation */
/* @var $form yii\widgets\ActiveForm */
/* @var $id integer */

$this->title = 'Добавить станцию';
$this->params['breadcrumbs'][] = ['label' => 'Рейсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Yii::getAlias('@web') . '/js/station.js', ['depends' => [yii\web\JqueryAsset::className()]]);

?>
<div class="create-route-form">
    <p>Название станции

    <?= AutoComplete::widget([
//    'model' => 'title',
//    'attribute' => 'title',
        'name' => 'station',
        'id' => 'auto-station',
        'options' => ['class' => 'form-control'],
        'clientOptions' => [
            'source' => Url::to(['route/view-station']),
            'minLength'=>'2',
        ],
    ]); ?>
    </p>
    <p>Растояние (в км)
    <?= Html::textInput('length','',['id' => 'length','class' => 'form-control']) ?>
    </p>
    <p>Время отправления
    <?= Html::textInput('time','',['id' => 'time','class' => 'form-control']) ?>
    </p>
    <p>Время прибытия
        <?= Html::textInput('arrival_time','',['id' => 'arrival_time','class' => 'form-control']) ?>
    </p>
    <p>
    <?= Html::button('Добавить',['class' => 'btn btn-info', 'id' => 'add-station', 'data-id' => $id]) ?>
    <?= Html::a('Создать новую остановку',['/route/station','id' => $id],['class' => 'btn btn-success']) ?>
    </p>
    <div class="view-station-route" id="listView">

        <?php Pjax::begin(['id' => 'stationListPjax']) ?>

        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'options' => [
                'data' => ['pjax' => true]
            ],
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model) {
                return $this->render('_data_station',[
                    'model' => $model,
                ]);
            },
//            'viewParams' => [
//                'fullView' => true,
//                'context' => 'main-page',
//            ]
        ]) ?>

        <?php Pjax::end() ?>
    </div>

</div>
