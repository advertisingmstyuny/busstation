<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Route */

$this->title = 'Создать рейс';
$this->params['breadcrumbs'][] = ['label' => 'Рейсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="route-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
