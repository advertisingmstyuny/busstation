<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.03.2018
 * Time: 22:04
 */

/* @var $model \backend\models\RouteStation */

?>

<div class="station-list" data-id="<?= $model->id ?>">
    <button type="button" class="close station-delete-list" id="deleteStation" data-id="<?= $model->id ?>"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="station-text">
        <table class="station_table">
            <tr>
                <th>Станция</th>
                <th>Километраж</th>
                <th>Время отправления</th>
                <th>Время прибытия</th>
            </tr>
            <tr>
                <td><?= $model->station->title ?></td>
                <td><?= $model->length ?></td>
                <td><?= $model->time ?></td>
                <td><?= $model->arrival_time ?></td>
            </tr>
        </table>
<!--        <p> | --><?//= $model->length ?><!-- | --><?//= $model->time ?><!-- | --><?//= $model->arrival_time ?><!--</p>-->
    </div>
</div>
