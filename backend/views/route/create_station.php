<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.03.2018
 * Time: 18:00
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $this \yii\web\view */
/* @var $model \backend\models\Station */
/* @var $form yii\widgets\ActiveForm */

?>
<div class="station-form">
    <p>Пожалуйста, введите название станции</p>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>

    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
