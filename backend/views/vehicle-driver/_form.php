<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Driver;
use yii\helpers\ArrayHelper;
use backend\models\Vehicle;

/* @var $this yii\web\View */
/* @var $model backend\models\VehicleDriver */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vehicle-driver-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_driver')->dropDownList(ArrayHelper::map(Driver::find()->all(), 'id', 'full_name'),[
        'prompt' => 'Водитель',
    ]) ?>

    <?= $form->field($model, 'id_vehicle')->dropDownList(ArrayHelper::map(Vehicle::find()->all(), 'id', 'vehicle'),[
        'prompt' => 'Автомобиль',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
