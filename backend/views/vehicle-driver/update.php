<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VehicleDriver */

$this->title = 'Update Vehicle Driver: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Drivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vehicle-driver-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
