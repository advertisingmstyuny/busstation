<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\VehicleDriver */

$this->title = 'Create Vehicle Driver';
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Drivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-driver-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
