<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Departure */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="departure-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_vehicle_driver')->textInput() ?>

    <?= $form->field($model, 'id_schedule')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
