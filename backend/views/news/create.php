<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.03.2018
 * Time: 15:26
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\view */
/* @var $form \yii\widgets\ActiveForm */
/* @var $news[] string */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
        'action' => ['news/create']
    ]); ?>

        <?= Html::textInput('news', '', ['class' => 'form-control']) ?>



    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php $form = ActiveForm::end(); ?>
</div>