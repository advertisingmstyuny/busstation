<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Busstation',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if (Yii::$app->user->isGuest):
        $guestItems[] = ['label' => 'Login', 'url' => ['/main/login']];
        $guestItems[] = ['label' => 'Sign up', 'url' => ['/main/signup']];
        echo Nav::widget([
            'items' => $guestItems,
            'encodeLabels' => false,
            'options' => [
                'class' => 'navbar-nav navbar-right'
            ]
        ]);
    else:
        $tableItems[] = [
            'label' => '<span class="glyphicon glyphicon-th-list"></span> Таблицы',
            'items' => [
                '<li class="dropdown-header">Таблицы</li>',
                '<li class="divider"></li>',
                [
                    'label' => '<span class="glyphicon glyphicon-user"></span> Заказчик',
                    'url' => ['/customer/index'],
                    'linkOptions' => ['data-method' => 'post']
                ],
                [
                    'label' => '<span class="glyphicon glyphicon-list-alt"></span> Заказы',
                    'url' => ['/order/index'],
                    'linkOptions' => ['data-method' => 'post']
                ],
                [
                    'label' => '<span class="glyphicon glyphicon-refresh"></span> Возврат билетов',
                    'url' => ['/return-ticket'],
                    'linkOptions' => ['data-method' => 'post']
                ],
                [
                    'label' => '<span class="glyphicon glyphicon-user"></span> Профиль',
                    'url' => ['/profile/index'],
                    'linkOptions' => ['data-method' => 'post']
                ],
                [
                    'label' => '<span class="glyphicon glyphicon-road"></span> Маршруты',
                    'url' => ['/route/index'],
                    'linkOptions' => ['data-method' => 'post']
                ],
                [
                    'label' => '<span class="glyphicon glyphicon-calendar"></span> Расписание',
                    'url' => ['/schedule/index'],
                    'linkOptions' => ['data-method' => 'post']
                ],
                [
                    'label' => '<span class="glyphicon glyphicon-user"></span> Водители',
                    'url' => ['/driver/index'],
                    'linkOptions' => ['data-method' => 'post']
                ],
                [
                    'label' => '<span class="glyphicon glyphicon-wrench"></span> Автомобили',
                    'url' => ['/vehicle/index'],
                    'linkOptions' => ['data-method' => 'post']
                ],
                [
                    'label' => '<span class="glyphicon glyphicon-heart"></span> Связь: Автомобиль - Водитель',
                    'url' => ['/vehicle-driver/index'],
                    'linkOptions' => ['data-method' => 'post']
                ],
                [
                    'label' => '<span class="glyphicon glyphicon-heart"></span> Новости',
                    'url' => ['/news-page/index'],
                    'linkOptions' => ['data-method' => 'post']
                ],
            ]
        ];
        Yii::setAlias('@goApp','http://busstation.loc/');
        $menuItems[] = [
            'label' => '<span class="glyphicon glyphicon-user"></span>',
            'items' => [
                '<li class="dropdown-header">'.Yii::$app->user->identity['username'].'</li>',
                '<li class="divider"></li>',
                [
                    'label' => '<span class="glyphicon glyphicon-home"></span> На сайт',
                    'url' => '@goApp',
                ],
                [
                    'label' => '<span class="glyphicon glyphicon-log-out"></span> Выход',
                    'url' => ['/main/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ]
            ]
        ];
        echo Nav::widget([
            'items' => $tableItems,
            'encodeLabels' => false,
            'options' => [
                'class' => 'navbar-nav navbar-left'
            ]
        ]);
        echo Nav::widget([
            'items' => $menuItems,
            'encodeLabels' => false,
            'options' => [
                'class' => 'navbar-nav navbar-right'
            ]
        ]);
    endif;
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
