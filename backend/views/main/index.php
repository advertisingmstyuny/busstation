<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05.03.2018
 * Time: 13:59
 */

use yii\bootstrap\Html;

/* @var $this \yii\web\view */

$this->title = 'Администратор';

?>

    <p><?= Html::a('Создать рейс', ['/route/create'], ['class' => 'btn btn-info']); ?></p>
    <p><?= Html::a('Добавить расписание', ['/schedule/create'], ['class' => 'btn btn-info']); ?></p>
    <p><?= Html::a('Добавить новость', ['/news-page/create'], ['class' => 'btn btn-info']); ?></p>