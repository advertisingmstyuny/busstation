<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ReturnTicket */

$this->title = 'Оформить возврат билета';
$this->params['breadcrumbs'][] = ['label' => 'Возврат билетов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="return-ticket-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
