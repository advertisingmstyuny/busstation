<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ReturnTicket */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="return-ticket-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number_ticket')->textInput() ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number_card')->textInput() ?>

    <?= $form->field($model, 'date_pay')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
