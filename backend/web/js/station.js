$('#add-station').on('click', function () {
    var route_id = $(this).data('id');
    var station = $('#auto-station').val();
    var length = $('#length').val();
    var time = $('#time').val();
    var arrival_time = $('#arrival_time').val();
    console.log(time);
    $.ajax({
        url: '/route/add-station',
        data: { route_id: route_id, station: station, length: length, time: time, arrival_time: arrival_time},
        type: "POST",
        success: function(res){
            $('#auto-station').val('');
            $('#length').val('');
            $('#time').val('');
            $('#arrival_time').val('');
            $('#listView').append(res);
            $('#listView > .station-list:first-of-type .station-delete-list').on('click', deleteStation);
        },
        error: function () {

        }
    });
});

$('.station-list').on('click', function () {
    var id = $(this).data('id');
    console.log(id)
});

$('.station-delete-list').on('click',deleteStation);

function deleteStation() {
    var id = $('.station-list').data('id');
    console.log(id);
    $.ajax(
        {
            url: '/route/delete-station',
            type: "POST",
            data : {id:id},
            success: function(res) {
            },
            error: function(res) {
            }
        }
    );
    $(this).parent().parent().remove();
}

