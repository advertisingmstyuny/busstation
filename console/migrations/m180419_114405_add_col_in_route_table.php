<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m180419_114405_add_col_in_route_table
 */
class m180419_114405_add_col_in_route_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('route','number',Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn('route', 'number');
        return false;
    }

}
