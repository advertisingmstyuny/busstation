<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180305_114401_add_column_in_vehicle_table
 */
class m180305_114401_add_column_in_vehicle_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('vehicle','model', Schema::TYPE_STRING.' NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('vehicle','model');

        return false;
    }

}
