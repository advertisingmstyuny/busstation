<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation for table `route`.
 */
class m180226_122252_create_route_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('route', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING.' NOT NULL',
            'route' => Schema::TYPE_STRING.'(8192) NOT NULL',
            'length' => Schema::TYPE_DOUBLE.' NOT NULL',
            'price' => Schema::TYPE_DOUBLE.' NOT NULL'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('route');
    }
}
