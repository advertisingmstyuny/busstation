<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m180428_140446_insert_col_number_card_in_return_ticket_table
 */
class m180428_140446_insert_col_number_card_in_return_ticket_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->alterColumn('return_ticket','number_card', Schema::TYPE_STRING.' NOT NULL');
    }

    public function down()
    {
        $this->alterColumn('return_ticket','number_card', Schema::TYPE_INTEGER.' NOT NULL');
    }

}
