<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `bank`.
 */
class m180424_124925_create_bank_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bank', [
            'id' => Schema::TYPE_PK,
            'full_name' => Schema::TYPE_STRING.' NOT NULL',
            'number_card' => Schema::TYPE_INTEGER.' NOT NULL',
            'money' => Schema::TYPE_DOUBLE,
            'created_at' => Schema::TYPE_STRING
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bank');
    }
}
