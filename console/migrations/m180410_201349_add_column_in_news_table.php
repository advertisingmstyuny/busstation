<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180410_201349_add_column_in_news_table
 */
class m180410_201349_add_column_in_news_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('news','status',Schema::TYPE_SMALLINT);
    }

    public function down()
    {
        $this->dropColumn('news','status');

        return false;
    }
}
