<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `station`.
 */
class m180312_125908_create_station_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('station', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING.' NOT NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('station');
    }
}
