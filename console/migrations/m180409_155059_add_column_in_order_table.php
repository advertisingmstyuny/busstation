<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180409_155059_add_column_in_order_table
 */
class m180409_155059_add_column_in_order_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('order','departure_date', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('order', 'departure_date');

        return false;
    }

}
