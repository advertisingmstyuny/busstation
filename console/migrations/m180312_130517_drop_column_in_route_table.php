<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the dropping of table `column_in_route`.
 */
class m180312_130517_drop_column_in_route_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('route','route');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('route','route',Schema::TYPE_STRING.'(8192) NOT NULL');
    }
}
