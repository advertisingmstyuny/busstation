<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180410_185419_add_column_in_order_profile_table
 */
class m180410_185419_add_column_in_order_profile_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('order_profile','status', Schema::TYPE_SMALLINT);
    }

    public function down()
    {
        $this->dropColumn('order_profile','status');

        return false;
    }
}
