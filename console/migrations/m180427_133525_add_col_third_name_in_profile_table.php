<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m180427_133525_add_col_third_name_in_profile_table
 */
class m180427_133525_add_col_third_name_in_profile_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('profile','third_name', Schema::TYPE_STRING.' NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('profile', 'third_name');
    }
}
