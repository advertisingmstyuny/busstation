<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m180427_123550_add_col_phone_in_return_ticket_table
 */
class m180427_123550_add_col_phone_in_return_ticket_table extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('return_ticket', 'phone', Schema::TYPE_STRING.' NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('return_ticket', 'phone');

        return false;
    }

}
