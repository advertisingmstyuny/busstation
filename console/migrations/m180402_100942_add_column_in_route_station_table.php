<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180402_100942_add_column_in_route_station_table
 */
class m180402_100942_add_column_in_route_station_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('route_station','length', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn('route_station','length');

        return false;
    }

}
