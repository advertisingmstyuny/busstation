<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation for table `customer`.
 */
class m180226_122331_create_customer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer', [
            'id' => Schema::TYPE_PK,
            'full_name' => Schema::TYPE_STRING.'(512) NOT NULL',
            'email' => Schema::TYPE_STRING.' NOT NULL',
            'phone_number' => Schema::TYPE_STRING.' NOT NULL',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('customer');
    }
}
