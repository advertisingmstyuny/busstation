<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles the dropping of table `col_in_schedule`.
 */
class m180419_113814_drop_col_in_schedule_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('schedule','departure_time');
        $this->dropColumn('schedule','arrival_time');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('schedule', 'departure_time', Schema::TYPE_STRING);
        $this->addColumn('schedule', 'arrival_time', Schema::TYPE_STRING);
    }
}
