<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation for table `vehicle_driver`.
 */
class m180226_124016_create_vehicle_driver_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('vehicle_driver', [
            'id' => Schema::TYPE_PK,
            'id_driver' => Schema::TYPE_INTEGER,
            'id_vehicle' => Schema::TYPE_INTEGER
        ]);

        $this->addForeignKey('key_driver_vehicle','vehicle_driver','id_driver','driver','id','cascade','cascade');
        $this->addForeignKey('key_vehicle_driver','vehicle_driver','id_vehicle','vehicle','id','cascade','cascade');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('key_vehicle_driver','vehicle_driver');
        $this->dropForeignKey('key_driver_vehicle','vehicle_driver');
        $this->dropTable('vehicle_driver');
    }
}
