<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation for table `driver`.
 */
class m180226_122155_create_driver_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('driver', [
            'id' => Schema::TYPE_PK,
            'full_name' => Schema::TYPE_STRING.'(512) NOT NULL',
            'phone_number' => Schema::TYPE_STRING.' NOT NULL',
            'address' => Schema::TYPE_STRING.'(256) NOT NULL',
            'create_at' => Schema::TYPE_STRING.' NOT NULL'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('driver');
    }
}
