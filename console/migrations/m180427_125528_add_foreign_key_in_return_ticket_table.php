<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m180427_125528_add_foreign_key_in_return_ticket_table
 */
class m180427_125528_add_foreign_key_in_return_ticket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('key_number_ticket_order', 'return_ticket', 'number_ticket', 'order','id', 'cascade','cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('key_number_ticket_order', 'return_ticket');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180427_125528_add_foreign_key_in_return_ticket_table cannot be reverted.\n";

        return false;
    }
    */
}
