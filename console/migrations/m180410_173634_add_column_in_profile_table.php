<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180410_173634_add_column_in_profile_table
 */
class m180410_173634_add_column_in_profile_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('profile','phone_number', Schema::TYPE_STRING.' NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('profile','phone_number');

        return false;
    }

}
