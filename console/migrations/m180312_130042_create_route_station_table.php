<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `route_station`.
 */
class m180312_130042_create_route_station_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('route_station', [
            'id' => Schema::TYPE_PK,
            'id_route' => Schema::TYPE_INTEGER,
            'id_station' => Schema::TYPE_INTEGER,
        ]);

        $this->addForeignKey('key_route_station','route_station','id_route','route','id','cascade','cascade');
        $this->addForeignKey('key_station_route','route_station','id_station','station','id','cascade','cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('key_station_route','route_station');
        $this->dropForeignKey('key_route_station','route_station');
        $this->dropTable('route_station');
    }
}
