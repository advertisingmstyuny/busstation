<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180416_165352_alter_column_place_in_order_table
 */
class m180416_165352_alter_column_place_in_order_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->alterColumn('order','place',Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->alterColumn('order','place', Schema::TYPE_INTEGER);
        return false;
    }
}
