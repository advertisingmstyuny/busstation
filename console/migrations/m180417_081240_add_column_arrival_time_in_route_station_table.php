<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180417_081240_add_column_arrival_time_in_route_station_table
 */
class m180417_081240_add_column_arrival_time_in_route_station_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('route_station','arrival_time', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('route_station','arrival_time');

        return false;
    }
}
