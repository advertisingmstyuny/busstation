<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `order_profile`.
 */
class m180410_174102_create_order_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_profile', [
            'id' => Schema::TYPE_PK,
            'id_order' => Schema::TYPE_INTEGER,
            'id_profile' => Schema::TYPE_INTEGER
        ]);

        $this->addForeignKey('key_id_order_profile', 'order_profile','id_order','order','id','cascade', 'cascade');
        $this->addForeignKey('key_order_id_profile', 'order_profile','id_profile','profile','id','cascade', 'cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('key_order_id_profile', 'order_profile');
        $this->dropForeignKey('key_id_order_profile','order_profile');
        $this->dropTable('order_profile');
    }
}
