<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation for table `departure`.
 */
class m180226_125228_create_departure_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('departure', [
            'id' => Schema::TYPE_PK,
            'id_vehicle_driver' => Schema::TYPE_INTEGER,
            'id_schedule' => Schema::TYPE_INTEGER
        ]);

        $this->addForeignKey('key_vehicle_driver_','departure','id_vehicle_driver','vehicle_driver','id');
        $this->addForeignKey('key_schedule_departure','departure','id_schedule','schedule','id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('key_vehicle_driver_','departure');
        $this->dropForeignKey('key_schedule_departure','departure');
        $this->dropTable('departure');
    }
}
