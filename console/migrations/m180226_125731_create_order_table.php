<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation for table `order`.
 */
class m180226_125731_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('order', [
            'id' => Schema::TYPE_PK,
            'id_customer' => Schema::TYPE_INTEGER,
            'id_schedule' => Schema::TYPE_INTEGER,
            'station_departure' => Schema::TYPE_STRING.' NOT NULL',
            'station_arrival' => Schema::TYPE_STRING.' NOT NULL',
            'create_at' => Schema::TYPE_STRING.' NOT NULL',
            'place' => Schema::TYPE_INTEGER.' NOT NULL',
            'end_price' => Schema::TYPE_DOUBLE.' NOT NULL'
        ]);

        $this->addForeignKey('key_order_customer','order','id_customer','customer','id');
        $this->addForeignKey('key_order_schedule','order','id_schedule','schedule','id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('key_order_schedule','order');
        $this->dropForeignKey('key_order_customer','order');
        $this->dropTable('order');
    }
}
