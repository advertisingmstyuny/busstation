<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation for table `profile`.
 */
class m180226_121506_create_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('profile', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'first_name' => Schema::TYPE_STRING.' NOT NULL',
            'second_name' => Schema::TYPE_STRING.' NOT NULL',
            'sex' => Schema::TYPE_SMALLINT.' NOT NULL',
            'birthday' => Schema::TYPE_STRING.' NOT NULL'
        ]);

        $this->addForeignKey('profile_user','profile','user_id','user','id','cascade','cascade');
    }


    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('profile_user','profile');
        $this->dropTable('profile');
    }
}
