<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180410_195654_add_column_in_news_table
 */
class m180410_195654_add_column_in_news_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('news','image',Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('news','image');

        return false;
    }

}
