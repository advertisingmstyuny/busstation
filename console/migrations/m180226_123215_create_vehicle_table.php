<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation for table `vehicle`.
 */
class m180226_123215_create_vehicle_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('vehicle', [
            'id' => Schema::TYPE_PK,
            'vehicle_number' => Schema::TYPE_STRING.' NOT NULL',
            'vehicle' => Schema::TYPE_STRING.' NOT NULL',
            'count_places' => Schema::TYPE_INTEGER.' NOT NULL'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('vehicle');
    }
}
