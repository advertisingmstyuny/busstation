<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `return_ticket`.
 */
class m180301_140047_create_return_ticket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('return_ticket', [
            'id' => Schema::TYPE_PK,
            'number_ticket' => Schema::TYPE_INTEGER.' NOT NULL',
            'full_name' => Schema::TYPE_STRING.'(512) NOT NULL',
            'number_card' => Schema::TYPE_INTEGER.' NOT NULL',
            'date_pay' => Schema::TYPE_STRING.' NOT NULL',
            'created_at' => Schema::TYPE_STRING,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('return_ticket');
    }
}
