<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180315_132707_add_columns_in_schedule_table
 */
class m180315_132707_add_columns_in_schedule_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('schedule','mode',Schema::TYPE_SMALLINT.' DEFAULT 0');
        $this->addColumn('schedule','from_date',Schema::TYPE_STRING);
        $this->addColumn('schedule','to_date', Schema::TYPE_STRING);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('schedule', 'to_date');
        $this->dropColumn('schedule', 'from_date');
        $this->dropColumn('schedule', 'mode');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180315_132707_add_columns_in_schedule_table cannot be reverted.\n";

        return false;
    }
    */
}
