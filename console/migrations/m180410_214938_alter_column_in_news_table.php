<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180410_214938_alter_column_in_news_table
 */
class m180410_214938_alter_column_in_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->alterColumn('news','title', Schema::TYPE_STRING.'(512) ');
        $this->alterColumn('news','description',Schema::TYPE_STRING.'(16384) ');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->alterColumn('news','title', Schema::TYPE_STRING.'(512) NOT NULL');
        $this->alterColumn('news','description',Schema::TYPE_STRING.'(16384) NOT NULL');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180410_214938_alter_column_in_news_table cannot be reverted.\n";

        return false;
    }
    */
}
