<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m180424_122335_add_col_status_in_order_table
 */
class m180424_122335_add_col_status_in_order_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('order', 'status', Schema::TYPE_SMALLINT.' DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('order', 'status');

        return false;
    }
}
