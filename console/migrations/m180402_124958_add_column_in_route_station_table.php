<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180402_124958_add_column_in_route_station_table
 */
class m180402_124958_add_column_in_route_station_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('route_station','time',Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('route_station','time');

        return false;
    }
}
