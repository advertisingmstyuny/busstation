<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation for table `schedule`.
 */
class m180226_124554_create_schedule_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('schedule', [
            'id' => Schema::TYPE_PK,
            'id_route' => Schema::TYPE_INTEGER,
            'id_vehicle_driver' => Schema::TYPE_INTEGER,
            'departure_time' => Schema::TYPE_STRING.' NOT NULL',
            'arrival_time' => Schema::TYPE_STRING.' NOT NULL'
        ]);

        $this->addForeignKey('key_route_schedule','schedule','id_route','route','id','cascade','cascade');
        $this->addForeignKey('key_vehicle_schedule','schedule','id_vehicle_driver','vehicle_driver','id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('key_route_schedule','schedule');
        $this->dropForeignKey('key_vehicle_schedule','schedule');
        $this->dropTable('schedule');
    }
}
